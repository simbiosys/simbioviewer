/*=========================================================================

Program:   Medical Imaging & Interaction Toolkit
Language:  C++
Date:      $Date$
Version:   $Revision$

Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#ifndef QMITKSimbioViewerAppLICATION_H_
#define QMITKSimbioViewerAppLICATION_H_

#include <berryIApplication.h>

class QmitkSimbioViewerApplication : public QObject, public berry::IApplication
{
  Q_OBJECT
  Q_INTERFACES(berry::IApplication)

public:

  QmitkSimbioViewerApplication();
  QmitkSimbioViewerApplication(const QmitkSimbioViewerApplication& other);

  QVariant Start(berry::IApplicationContext* context);
  void Stop();
};

#endif /*QMITKSimbioViewerAppLICATION_H_*/
