PROJECT(my_SimbioViewerProj_renderwindoweditor)

mitk_create_plugin(
  EXPORT_DIRECTIVE MY_SimbioViewerProj_RENDERWINDOWEDITOR
  EXPORTED_INCLUDE_SUFFIXES src
  MODULE_DEPENDS MitkQtWidgets
  SUBPROJECTS MITK-CoreUI
  )
