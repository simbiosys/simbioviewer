# ===================================================================================
#  aruco CMake configuration file
#
#             ** File generated automatically, do not modify **
#
#  Usage from an external project:
#    In your CMakeLists.txt, add these lines:
#
#    FIND_PACKAGE(aruco REQUIRED )
#    TARGET_LINK_LIBRARIES(MY_TARGET_NAME )
#
#    This file will define the following variables:
#      - aruco_LIBS          : The list of libraries to links against.
#      - aruco_LIB_DIR       : The directory where lib files are. Calling LINK_DIRECTORIES
#                                with this path is NOT needed.
#      - aruco_VERSION       : The  version of this PROJECT_NAME build. Example: "1.2.0"
#      - aruco_VERSION_MAJOR : Major version part of VERSION. Example: "1"
#      - aruco_VERSION_MINOR : Minor version part of VERSION. Example: "2"
#      - aruco_VERSION_PATCH : Patch version part of VERSION. Example: "0"
#
# ===================================================================================

find_path(aruco_INCLUDE_DIR aruco.h
          PATHS 
            /home/albertalises/Documents/aruco/src
            /home/mario/Downloads/aruco-2.0.19/src
            /home/rlopez/opt/lib/aruco-2.0.19/src
          )

find_path(aruco_CALIB_INCLUDE_DIR aruco_calibration_board_a4.h
          PATHS 
            /home/albertalises/Documents/aruco/utils_calibration
            /home/mario/Downloads/aruco-2.0.19/utils_calibration
            /home/rlopez/opt/lib/aruco-2.0.19/utils_calibration
          )

find_path(aruco_LIB_DIR libaruco.so
          PATHS 
            /home/albertalises/Documents/aruco/build-release/src
            /home/mario/Downloads/aruco-2.0.19/build/src
            /home/rlopez/opt/lib/aruco-2.0.19/build-release/src
          )

SET(aruco_INCLUDE_DIRS ${aruco_INCLUDE_DIR} ${aruco_CALIB_INCLUDE_DIR})

SET(aruco_LIBS opencv_videostab;opencv_video;opencv_ts;opencv_superres;opencv_stitching;opencv_photo;opencv_ocl;opencv_objdetect;opencv_ml;opencv_legacy;opencv_imgproc;opencv_highgui;opencv_gpu;opencv_flann;opencv_features2d;opencv_core;opencv_contrib;opencv_calib3d aruco)

SET(aruco_FOUND 1)
SET(aruco_VERSION        2.0.19)
SET(aruco_VERSION_MAJOR  2)
SET(aruco_VERSION_MINOR  0)
SET(aruco_VERSION_PATCH  19)
