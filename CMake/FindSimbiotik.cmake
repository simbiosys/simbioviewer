# - Try to find Simbiotik
# Once done this will define
#  SIMBIOTIK_FOUND - System has Simbiotik
#  SIMBIOTIK_INCLUDE_DIRS - The Simbiotik include directories
#  SIMBIOTIK_LIBRARIES - The libraries needed to use Simbiotik

find_path(SMBTIK_SR_INC ResamplingImageFilter.h
          PATHS 
            /home/albertalises/git/simbiotik/includes/
            /home/mario/git/Featured/simbiotik/includes/
            /home/rlopez/git/simbiotik/includes/
          )
find_path(SMBTIK_TR_INC ArucoTracking.h
          PATHS 
            /home/albertalises/git/simbiotik/includes/
            /home/mario/git/Featured/simbiotik/includes/
            /home/rlopez/git/simbiotik/includes/

          )
find_library(SMBTIK_SR 
            NAMES Superresolution 
            PATHS 
            /home/albertalises/git/simbiotik/build-release/superresolution/
            /home/mario/git/Featured/simbiotik/build-release/superresolution/
	    /home/rlopez/git/simbiotik/build-release/superresolution/ )

find_library(SMBTIK_TR 
            NAMES Tracking 
            PATHS 
            /home/albertalises/git/simbiotik/build-release/tracking/
            /home/mario/git/Featured/simbiotik/build-release/tracking/
            /home/rlopez/git/simbiotik/build-release/tracking/ )

set(SIMBIOTIK_INCLUDE_DIRS ${SMBTIK_SR_INC} ${SMBTIK_TR_INC})
set(SIMBIOTIK_LIBRARIES ${SMBTIK_SR} ${SMBTIK_TR})

SET(SIMBIOTIK_FOUND 0)
IF(SIMBIOTIK_INCLUDE_DIRS)
  IF(SIMBIOTIK_LIBRARIES)
    SET(SIMBIOTIK_FOUND 1)
    MESSAGE(STATUS "Found SimBioTIK")
    MESSAGE(STATUS "Libraries: " ${SIMBIOTIK_LIBRARIES})
    MESSAGE(STATUS "Includes: " ${SIMBIOTIK_INCLUDE_DIRS})
  ENDIF(SIMBIOTIK_LIBRARIES)
ENDIF(SIMBIOTIK_INCLUDE_DIRS)

